/**
 * JavaScript Queue
 *
 * This is an implementation of the queue data structure in JavaScript.
 * queued can be implemented using a Singly linked list.
 */

import {DoublyLinkedList} from 'linkedlists';

export default class Queue {
    constructor() {
        this._linkedList = new DoublyLinkedList();
    }
    /**
     * Push/Insert/Add a value to the queue
     *
     * @param value {Object} node value
     */
    push = value => {
        this._linkedList.insertHead(value);
    }
    /**
     * Delete/Pop the top node from the queue
     * The method returns null if the queue is empty; hence, if you are going to use the returned value,
     * always check if the queue is empty or not as 'null' might also represent an actual node value.
     *
     * @returns value {Object} value of the popped node. null if empty.
     */
    pop = () => {
        let value = null;
        const tailNode = this._linkedList.tail;
        if (tailNode !== null) {
            value = tailNode.value;
            tailNode.delete();
        }
        return value;
    }
    /**
     * Check if the queue is empty or not
     *
     * @returns isEmpty {Boolean} true if list is empty, false if not
     */
    isEmpty = () => this._linkedList.size === 0
    /**
     * Return the array representation of the queue
     *
     * @returns arrayRep {Array} Array representation of the queue
     */
    toArray = () => {
        if (this.isEmpty()) {
            return null;
        }
        let parentNode = this._linkedList.tail;
        const arrayRep = [];
        while (parentNode !== null) {
            arrayRep.push(parentNode.value);
            parentNode = parentNode.previous;
        }
        return arrayRep;
    }
    /**
     * Get the head element value
     *
     * @returns value {Object} value if tail element exists, null if not
     */
    get head() {
        if (this.isEmpty()) {
            return null;
        }
        return this._linkedList.tail.value;
    }
    /**
     * Get the size of the queue
     *
     * @returns size {Int} size of the queue
     */
    get size() {
        return this._linkedList.size;
    }
}
