'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }(); /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      * JavaScript Queue
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      *
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      * This is an implementation of the queue data structure in JavaScript.
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      * queued can be implemented using a Singly linked list.
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      */

var _linkedlists = require('linkedlists');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Queue = function () {
    function Queue() {
        var _this = this;

        _classCallCheck(this, Queue);

        this.push = function (value) {
            _this._linkedList.insertHead(value);
        };

        this.pop = function () {
            var value = null;
            var tailNode = _this._linkedList.tail;
            if (tailNode !== null) {
                value = tailNode.value;
                tailNode.delete();
            }
            return value;
        };

        this.isEmpty = function () {
            return _this._linkedList.size === 0;
        };

        this.toArray = function () {
            if (_this.isEmpty()) {
                return null;
            }
            var parentNode = _this._linkedList.tail;
            var arrayRep = [];
            while (parentNode !== null) {
                arrayRep.push(parentNode.value);
                parentNode = parentNode.previous;
            }
            return arrayRep;
        };

        this._linkedList = new _linkedlists.DoublyLinkedList();
    }
    /**
     * Push/Insert/Add a value to the queue
     *
     * @param value {Object} node value
     */

    /**
     * Delete/Pop the top node from the queue
     * The method returns null if the queue is empty; hence, if you are going to use the returned value,
     * always check if the queue is empty or not as 'null' might also represent an actual node value.
     *
     * @returns value {Object} value of the popped node. null if empty.
     */

    /**
     * Check if the queue is empty or not
     *
     * @returns isEmpty {Boolean} true if list is empty, false if not
     */

    /**
     * Return the array representation of the queue
     *
     * @returns arrayRep {Array} Array representation of the queue
     */


    _createClass(Queue, [{
        key: 'head',

        /**
         * Get the head element value
         *
         * @returns value {Object} value if tail element exists, null if not
         */
        get: function get() {
            if (this.isEmpty()) {
                return null;
            }
            return this._linkedList.tail.value;
        }
        /**
         * Get the size of the queue
         *
         * @returns size {Int} size of the queue
         */

    }, {
        key: 'size',
        get: function get() {
            return this._linkedList.size;
        }
    }]);

    return Queue;
}();

exports.default = Queue;