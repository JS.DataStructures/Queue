import chai from 'chai';
import Queue from '../src/Queue';

const expect = chai.expect;

describe('Queue', () => {
    let queue;
    beforeEach(() => {
        queue = new Queue();
    });
    describe('Insertion Tests', () => {
        it('should be able to construct', () => {
            expect(queue).be.not.null && expect(queue).be.not.undefined;
        });
        it('new queue should be empty', () => {
            expect(queue.isEmpty()).to.equal(true);
        });
        it('new element should result in size 1', () => {
            queue.push(1);
            expect(queue.size).to.equal(1);
        });
        it('new element should result in queue array containing 1', () => {
            queue.push(1);
            expect(queue.toArray().toString()).to.equal([1].toString());
        });
        it('new second element should result in size 1', () => {
            queue.push(1);
            queue.push(2);
            expect(queue.size).to.equal(2);
        });
        it('new second element should result in queue array containing 1 and 2', () => {
            queue.push(1);
            queue.push(2);
            expect(queue.toArray().toString()).to.equal([1,2].toString());
        });
    });
    describe('Deletion Tests', () => {
        it('Deleting the first and only element should result in size 0', () => {
            queue.push(1);
            queue.pop();
            expect(queue.size).to.equal(0);
        });
        it('Deleting the second element should result in size 1', () => {
            queue.push(1);
            queue.push(2);
            queue.pop();
            expect(queue.size).to.equal(1);
        });
        it('Deleting the second element should result in queue array containing 2', () => {
            queue.push(1);
            queue.push(2);
            queue.pop();
            expect(queue.toArray().toString()).to.equal([2].toString());
        });
    });
});
