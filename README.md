# Queues

## Installation

```
npm install queue
```

## Usage

```
import Queue from 'queue';
```

## API

```push (value): Enter a value into the queue```

```pop: Delete the top of the pop```

```isEmpty: boolean if the queue is empty or not```

```size: Get the size of the queue```

```head: Get the top most element of the queue```

```toArray: Get all the elements of the queue```